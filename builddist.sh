#!/usr/bin/env bash

set -e
npm run build

name=$(jq -r -M '.name' package.json)
vers=$(git describe --tags --always --dirty --match=v* 2> /dev/null || \
	   cat $(CURDIR)/.version 2> /dev/null || echo v0)
echo "${name}_${vers}"
cd dist

tar -c -v -z -f ../${name}_${vers}.tar.gz *
cd -
