// Produce "human friendly" file sizes

// Borrowed from: https://github.com/dustin/go-humanize/blob/master/bytes.go
export default function humanBytes(val) {
    let logn = function(n, b) {
        return Math.log(n)/Math.log(b);
    };

    let toint = function(x) {
        return Number(Math.floor(x)).toFixed(0);
    };


    if (val < 10) {
        return Number(val).toString() + " B";
    }
    let base = 1000;
    let sizes = ["B", "kB", "MB", "GB", "TB", "PB", "EB"];
    let e = Math.floor(logn(val, base));
    let suffix = sizes[toint(e)];
    val = Math.floor(val/Math.pow(base, e)*10+0.5) / 10;
    if (val < 10) {
        return Number(val).toFixed(1).toString() + " " + suffix;
    }
    return Number(val).toFixed(0).toString() + " " + suffix;
}
